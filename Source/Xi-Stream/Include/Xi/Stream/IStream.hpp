/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include <istream>

#include "Xi/Stream/IInputStream.hpp"

namespace Xi {
namespace Stream {
class IStream final : public IInputStream {
 public:
  explicit IStream(std::istream& source);
  XI_DELETE_COPY(IStream);
  XI_DEFAULT_MOVE(IStream);
  ~IStream() override = default;

  [[nodiscard]] size_t read(ByteSpan buffer) override;
  [[nodiscard]] bool isEndOfStream() const override;
  [[nodiscard]] size_t tell() const override;
  [[nodiscard]] Byte peek() const override;
  [[nodiscard]] Byte take() override;

 private:
  std::istream& m_source;
};

XI_DECLARE_SMART_POINTER(IStream)
}  // namespace Stream
}  // namespace Xi
