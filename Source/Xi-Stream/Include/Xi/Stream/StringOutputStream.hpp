/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <string>

#include <Xi/Global.hh>

#include "Xi/Stream/IOutputStream.hpp"

namespace Xi {
namespace Stream {
class StringOutputStream final : public IOutputStream {
 public:
  explicit StringOutputStream(std::string &dest);
  XI_DELETE_COPY(StringOutputStream);
  XI_DEFAULT_MOVE(StringOutputStream);
  ~StringOutputStream() override = default;

  [[nodiscard]] size_t write(ConstByteSpan buffer) override;
  [[nodiscard]] bool flush() override;

 private:
  std::string &m_dest;
};

XI_DECLARE_SMART_POINTER(StringOutputStream)
}  // namespace Stream
}  // namespace Xi
