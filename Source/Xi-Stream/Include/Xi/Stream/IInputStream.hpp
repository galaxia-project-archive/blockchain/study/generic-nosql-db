/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

namespace Xi {
namespace Stream {

/*!
 *
 * \brief The IInputStream interface declares all necessary interaction to read from an arbritrary
 * source of bytes.
 *
 * InputStreams should not hold any data. They rather wrap a source of data and provide an interface
 * to read from it.
 *
 */
class IInputStream {
 public:
  virtual ~IInputStream() = default;

  /*!
   *
   * \brief read reads bytes up till end of stream is reach of the provided buffer is filled.
   * \param buffer An output buffer to copy bytes read to.
   * \return A failure or actually bytes read.
   * \return zero on failure otherwise actually bytes read.
   *
   * \attention Always check the actual bytes read, its is not guaranteed to be fully filled, if you
   * require exaclty the number of bytes of the buffer size use readStrict.
   *
   */
  [[nodiscard]] virtual size_t read(ByteSpan buffer) = 0;

  /*!
   *
   * \brief isEndOfStream checks if the source stream is exhausted.
   * \return true if no byte can be read anymore, otherwise false.
   *
   * \note The caller of tell and peek has to check this first, otherwise behavious is undefined.
   *
   */
  [[nodiscard]] virtual bool isEndOfStream() const = 0;

  /*!
   *
   * \brief tell current position in the stream
   * \return index based offset of the current stream position, relative to its beginning, (size) if
   * endOfStream.
   *
   * \attention in case of compund streams this position may be invalid for the current buffer
   *
   */
  [[nodiscard]] virtual size_t tell() const = 0;

  /*!
   *
   * \brief take reads the next byte without moving the stream.
   * \return byte read
   *
   * \attention It is up to the caller to guarantee the stream can perform this action otherwise an
   * excpetions is thrown.
   *
   * \exceptions OutOfRange
   *
   */
  [[nodiscard]] virtual Byte peek() const = 0;

  /*!
   *
   * \brief take reads the next byte and advances the stream.
   * \return byte read
   *
   * \attention It is up to the caller to guarantee the stream can perform this action otherwise an
   * excpetions is thrown.
   *
   * \exceptions OutOfRange
   *
   */
  [[nodiscard]] virtual Byte take() = 0;
};

XI_DECLARE_SMART_POINTER(IInputStream)

/*!
 *
 * \brief readStrict repeates read operations until buffer is filled
 * \param stream to read from
 * \param buffer to fill
 * \return true on success otherwise false
 *
 * \note providing an empty buffer is well defined and will return true, reading no byte at all.
 *
 */
[[nodiscard]] bool readStrict(IInputStream &stream, ByteSpan buffer);

}  // namespace Stream
}  // namespace Xi
