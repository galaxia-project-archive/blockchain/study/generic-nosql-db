/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/ByteArrayOutputStream.hpp"

#include <cstring>
#include <algorithm>

Xi::Stream::ByteArrayOutputStream::ByteArrayOutputStream(Xi::ByteSpan dest) : m_dest{dest} {
}

size_t Xi::Stream::ByteArrayOutputStream::write(Xi::ConstByteSpan buffer) {
  XI_RETURN_EC_IF(m_dest.empty(), 0);
  const auto nWrite = std::min(buffer.size(), m_dest.size());
  std::memcpy(m_dest.data(), buffer.data(), nWrite);
  m_dest = m_dest.slice(buffer.size());
  m_written = ByteSpan{m_written.data(), m_written.size() + buffer.size()};
  return nWrite;
}

bool Xi::Stream::ByteArrayOutputStream::flush() {
  return true;
}

Xi::ByteSpan Xi::Stream::ByteArrayOutputStream::written() const {
  return m_written;
}
