/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/CompoundInputStream.hpp"

#include <utility>

#include <Xi/Exceptions.hpp>

Xi::Stream::CompoundInputStream::CompoundInputStream(std::vector<UniqueIInputStream> sources)
    : m_pos{0} {
  for (auto& source : sources) {
    if (source) {
      m_sources.emplace_back(std::move(source));
    }
  }
}

size_t Xi::Stream::CompoundInputStream::read(Xi::ByteSpan buffer) {
  popEndOfStreamSources();
  XI_RETURN_EC_IF(isEndOfStream(), 0);
  size_t nRead = 0;
  do {
    const size_t iRead = m_sources.front()->read(buffer);
    XI_RETURN_EC_IF(iRead == 0, 0);
    buffer = buffer.slice(iRead);
    nRead += iRead;
    if (buffer.empty()) {
      popEndOfStreamSources();
      return nRead;
    } else if (m_sources.front()->isEndOfStream()) {
      popEndOfStreamSources();
    } else {
      return nRead;
    }
  } while (!isEndOfStream());
  return nRead;
}

bool Xi::Stream::CompoundInputStream::isEndOfStream() const {
  for (const auto& iSource : m_sources) {
    XI_RETURN_EC_IF_NOT(iSource->isEndOfStream(), false);
  }
  return true;
}

size_t Xi::Stream::CompoundInputStream::tell() const {
  return m_pos;
}

Xi::Byte Xi::Stream::CompoundInputStream::peek() const {
  exceptional_if<OutOfRangeError>(isEndOfStream());
  for (const auto& iSource : m_sources) {
    XI_RETURN_EC_IF_NOT(iSource->isEndOfStream(), iSource->peek());
  }
  exceptional<OutOfRangeError>();
}

Xi::Byte Xi::Stream::CompoundInputStream::take() {
  popEndOfStreamSources();
  exceptional_if<OutOfRangeError>(isEndOfStream());
  const auto reval = m_sources.front()->take();
  m_pos += 1;
  return reval;
}

void Xi::Stream::CompoundInputStream::popEndOfStreamSources() {
  while (!m_sources.empty() && m_sources.front()->isEndOfStream()) {
    m_sources.pop_front();
  }
}
