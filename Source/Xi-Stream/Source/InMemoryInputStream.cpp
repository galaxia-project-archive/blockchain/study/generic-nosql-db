/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Stream/InMemoryInputStream.hpp"

#include <cstring>
#include <algorithm>

#include <Xi/Exceptions.hpp>

Xi::Stream::InMemoryInputStream::InMemoryInputStream(ConstByteSpan source)
    : m_source{source}, m_pos{0} {
}

size_t Xi::Stream::InMemoryInputStream::read(Xi::ByteSpan buffer) {
  if (isEndOfStream())
    return 0;

  size_t size = std::min(buffer.size(), m_source.size() - m_pos);
  std::memcpy(buffer.data(), m_source.data() + m_pos, static_cast<size_t>(size));
  m_pos += size;
  return static_cast<size_t>(size);
}

bool Xi::Stream::InMemoryInputStream::isEndOfStream() const {
  return tell() == m_source.size();
}

size_t Xi::Stream::InMemoryInputStream::tell() const {
  return m_pos;
}

Xi::Byte Xi::Stream::InMemoryInputStream::peek() const {
  exceptional_if<OutOfRangeError>(isEndOfStream(), "peek exceeded stream size");
  return m_source[m_pos];
}

Xi::Byte Xi::Stream::InMemoryInputStream::take() {
  exceptional_if<OutOfRangeError>(isEndOfStream(), "take exceeded stream size");
  return m_source[m_pos++];
}
