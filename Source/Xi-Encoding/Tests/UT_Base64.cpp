/* ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 *                                                   MIT Licenese *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Copyright 2019 Michael Herwig <michael.herwig@hotmail.de> *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated       * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation    * the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and   * to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:                 *
 *                                                                                                                    *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of   * the Software. *
 *                                                                                                                    *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO   * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF          * CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS *
 * IN THE SOFTWARE. *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 */

#include <gmock/gmock.h>

#include <Xi/Stream/InMemoryStreams.hpp>
#include <Xi/Encoding/Exceptions.hpp>
#include <Xi/Encoding/Base64.hpp>

namespace {
const std::string DecodedString{"This is a test string for Xi."};
const std::string EncodedString{"VGhpcyBpcyBhIHRlc3Qgc3RyaW5nIGZvciBYaS4="};
}  // namespace

#define XI_TEST_SUITE Xi_Encoding_Base64

TEST(XI_TEST_SUITE, Encode) {
  using namespace Xi::Encoding::Base64;
  using namespace Xi::Stream;

  {
    std::string encoded;
    ASSERT_TRUE(encode(*asInputStream(DecodedString), *asOutputStream(encoded)));
    EXPECT_EQ(encoded, EncodedString);
  }

  {
    std::string encoded;
    ASSERT_TRUE(encode(*asInputStream(""), *asOutputStream(encoded)));
    EXPECT_EQ(encoded, "");
  }
}

TEST(XI_TEST_SUITE, Decode) {
  using namespace Xi::Encoding::Base64;
  using namespace Xi::Stream;

  {
    std::string decoded;
    ASSERT_TRUE(decode(*asInputStream(EncodedString), *asOutputStream(decoded)));
    EXPECT_EQ(decoded, DecodedString);
  }

  {
    std::string decoded;
    ASSERT_TRUE(decode(*asInputStream(""), *asOutputStream(decoded)));
    EXPECT_EQ(decoded, "");
  }
}

TEST(XI_TEST_SUITE, DecodeFailures) {
  using namespace Xi::Encoding::Base64;
  using namespace Xi::Stream;
  using Xi::Encoding::EncodingException;

  std::string decoded;
  EXPECT_FALSE(decode(*asInputStream("5e7a3"), *asOutputStream(decoded)));
  EXPECT_FALSE(decode(*asInputStream("5$7a"), *asOutputStream(decoded)));
}
