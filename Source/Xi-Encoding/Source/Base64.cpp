/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Encoding/Base64.hpp"

#include <array>

#include <Xi/Global.hh>
#include <Xi/Stream/InMemoryStreams.hpp>

namespace {
static const std::array<std::string::value_type, 64> EncodingTable = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

static const std::array<Xi::Byte, 256> DecodingTable = {
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64, 64, 0,  1,  2,  3,  4,  5,  6,
    7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64,
    64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
    49, 50, 51, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64};

inline constexpr size_t BufferSize() {
  return 16;
}
}  // namespace

bool Xi::Encoding::Base64::encode(Xi::Stream::IInputStream &source,
                                  Xi::Stream::IOutputStream &dest) {
  ByteArray<3 * BufferSize()> inputBuffer;

  while (!source.isEndOfStream()) {
    auto size = source.read(inputBuffer);
    size_t i = 0;
    size_t j = 0;
    std::string ichunk;
    Byte const *bytes_to_encode = inputBuffer.data();
    Byte char_array_3[3];
    Byte char_array_4[4];

    while (size--) {
      char_array_3[i++] = *(bytes_to_encode++);
      if (i == 3) {
        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] =
            static_cast<Byte>((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] =
            static_cast<Byte>((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;

        for (i = 0; (i < 4); i++)
          ichunk += EncodingTable[char_array_4[i]];
        i = 0;
      }
    }

    if (i) {
      for (j = i; j < 3; j++)
        char_array_3[j] = '\0';

      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] =
          static_cast<Byte>((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] =
          static_cast<Byte>((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

      for (j = 0; (j < i + 1); j++)
        ichunk += EncodingTable[char_array_4[j]];

      while ((i++ < 3))
        ichunk += '=';
    }
    XI_RETURN_EC_IF_NOT(
        Stream::writeStrict(dest,
                            ConstByteSpan{reinterpret_cast<Byte *>(ichunk.data()), ichunk.size()}),
        false);
  }
  return true;
}

bool Xi::Encoding::Base64::decode(Xi::Stream::IInputStream &source,
                                  Xi::Stream::IOutputStream &dest) {
  ByteArray<4 * BufferSize()> inputBuffer;

  while (!source.isEndOfStream()) {
    size_t size = 0;
    do {
      XI_RETURN_EC_IF_NOT(Stream::readStrict(source, {inputBuffer.data() + size, 4}), false);
      size += 4;
    } while (!source.isEndOfStream() && size < inputBuffer.size());

    size_t out_len = size / 4 * 3;
    if (inputBuffer[size - 1] == '=')
      out_len--;
    if (inputBuffer[size - 2] == '=')
      out_len--;

    ByteVector chunk;
    chunk.resize(out_len);

    for (size_t i = 0, j = 0; i < size;) {
      uint32_t a = inputBuffer[i] == '=' ? 0 & i++ : DecodingTable[inputBuffer[i++]];
      XI_RETURN_EC_IF(a == 64, false);
      uint32_t b = inputBuffer[i] == '=' ? 0 & i++ : DecodingTable[inputBuffer[i++]];
      XI_RETURN_EC_IF(b == 64, false);
      uint32_t c = inputBuffer[i] == '=' ? 0 & i++ : DecodingTable[inputBuffer[i++]];
      XI_RETURN_EC_IF(c == 64, false);
      uint32_t d = inputBuffer[i] == '=' ? 0 & i++ : DecodingTable[inputBuffer[i++]];
      XI_RETURN_EC_IF(d == 64, false);

      uint32_t triple = (a << 3 * 6) + (b << 2 * 6) + (c << 1 * 6) + (d << 0 * 6);

      if (j < out_len)
        chunk[j++] = (triple >> 2 * 8) & 0xFF;
      if (j < out_len)
        chunk[j++] = (triple >> 1 * 8) & 0xFF;
      if (j < out_len)
        chunk[j++] = (triple >> 0 * 8) & 0xFF;
    }

    XI_RETURN_EC_IF_NOT(
        Stream::writeStrict(dest, ByteSpan{reinterpret_cast<Byte *>(chunk.data()), chunk.size()}),
        false);
  }
  return true;
}

std::string Xi::Encoding::Base64::encode(std::string_view source) {
  Stream::InMemoryInputStream input{asByteSpan(source)};
  std::string reval;
  Stream::StringOutputStream output{reval};
  XI_RETURN_EC_IF_NOT(encode(input, output), "");
  return reval;
}

std::string Xi::Encoding::Base64::decode(std::string_view source) {
  Stream::InMemoryInputStream input{asByteSpan(source)};
  std::string reval;
  Stream::StringOutputStream output{reval};
  XI_RETURN_EC_IF_NOT(decode(input, output), "");
  return reval;
}
