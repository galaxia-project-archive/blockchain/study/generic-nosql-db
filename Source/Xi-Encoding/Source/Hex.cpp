/* ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 *                                                   MIT Licenese *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Copyright 2019 Michael Herwig <michael.herwig@hotmail.de> *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated       * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation    * the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and   * to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:                 *
 *                                                                                                                    *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of   * the Software. *
 *                                                                                                                    *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO   * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF          * CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS *
 * IN THE SOFTWARE. *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 */

#include "Xi/Encoding/Hex.hpp"

namespace {
inline constexpr size_t BufferSize() {
  return 64;
}
static_assert((BufferSize() % 4) == 0 && BufferSize() > 0,
              "buffer size must be a multiple of 4 and none empty");

Xi::ByteArray<256> HexLookupMap{
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff

};
}  // namespace

bool Xi::Encoding::Hex::encode(Xi::Stream::IInputStream &source, Xi::Stream::IOutputStream &dest) {
  ByteArray<BufferSize()> inputBuffer;
  ByteArray<BufferSize() * 2> outputBuffer;

  while (!source.isEndOfStream()) {
    auto size = source.read(inputBuffer);
    for (size_t i = 0; i < size; ++i) {
      outputBuffer[i * 2 + 0] = static_cast<Byte>("0123456789abcdef"[inputBuffer[i] >> 4]);
      outputBuffer[i * 2 + 1] = static_cast<Byte>("0123456789abcdef"[inputBuffer[i] & 15]);
    }
    XI_RETURN_EC_IF_NOT(Stream::writeStrict(dest, ByteSpan{outputBuffer.data(), size * 2}), false);
  }
  return true;
}

bool Xi::Encoding::Hex::decode(Xi::Stream::IInputStream &source, Xi::Stream::IOutputStream &dest) {
  ByteArray<BufferSize() * 2> inputBuffer;
  ByteArray<BufferSize()> outputBuffer;

  while (!source.isEndOfStream()) {
    auto size = source.read(inputBuffer);
    XI_RETURN_EC_IF((size & 1) != 0, false);
    for (size_t i = 0; i < size; i += 2) {
      const Byte high = HexLookupMap[inputBuffer[i + 0]];
      XI_RETURN_EC_IF(high > 0x0f, false);
      const Byte low = HexLookupMap[inputBuffer[i + 1]];
      XI_RETURN_EC_IF(low > 0x0f, false);
      outputBuffer[i / 2] = static_cast<Byte>(high << 4) | low;
    }
    XI_RETURN_EC_IF_NOT(Stream::writeStrict(dest, ByteSpan{outputBuffer.data(), size / 2}), false);
  }
  return true;
}
