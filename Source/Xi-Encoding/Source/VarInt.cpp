/* ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 *                                                   MIT Licenese *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Copyright 2019 Michael Herwig <michael.herwig@hotmail.de> *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 * *
 *                                                                                                                    *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated       * documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation    * the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and   * to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:                 *
 *                                                                                                                    *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of   * the Software. *
 *                                                                                                                    *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO   * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF          * CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS *
 * IN THE SOFTWARE. *
 *                                                                                                                    *
 * ------------------------------------------------------------------------------------------------------------------
 */

#include "Xi/Encoding/VarInt.hh"

#include "Xi/Global.hh"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::VarInt, Decode)
XI_ERROR_CODE_DESC(Overflow, "decoding resulted in an overflow")
XI_ERROR_CODE_DESC(OutOfMemory, "decoding expected an successing byte, but reached end of stream")
XI_ERROR_CODE_DESC(NoneCanonical, "a trailing zero byte was encoded")
XI_ERROR_CODE_CATEGORY_END()

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::VarInt, Encode)
XI_ERROR_CODE_DESC(OutOfMemory, "end of stream reached writing encoded bytes")
XI_ERROR_CODE_CATEGORY_END()

Xi::Result<size_t> Xi::Encoding::VarInt::decode(const Xi::ByteSpan source, int16_t &out) {
  auto reval =
      xi_encoding_varint_decode_int16(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, failure(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, failure(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, failure(DecodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::decode(const Xi::ByteSpan source, uint16_t &out) {
  auto reval =
      xi_encoding_varint_decode_uint16(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, failure(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, failure(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, failure(DecodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::decode(const Xi::ByteSpan source, int32_t &out) {
  auto reval =
      xi_encoding_varint_decode_int32(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, failure(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, failure(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, failure(DecodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::decode(const Xi::ByteSpan source, uint32_t &out) {
  auto reval =
      xi_encoding_varint_decode_uint32(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, failure(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, failure(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, failure(DecodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::decode(const Xi::ByteSpan source, int64_t &out) {
  auto reval =
      xi_encoding_varint_decode_int64(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, failure(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, failure(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, failure(DecodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::decode(const Xi::ByteSpan source, uint64_t &out) {
  auto reval =
      xi_encoding_varint_decode_uint64(source.data(), static_cast<size_t>(source.size()), &out);
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OVERFLOW, failure(DecodeError::Overflow));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_NONE_CANONICAL, failure(DecodeError::NoneCanonical));
  XI_RETURN_EC_IF(reval == XI_VARINT_DECODE_OUT_OF_MEMORY, failure(DecodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::encode(int16_t value, Xi::ByteSpan dest) {
  auto reval =
      xi_encoding_varint_encode_int16(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, failure(EncodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::encode(uint16_t value, Xi::ByteSpan dest) {
  auto reval =
      xi_encoding_varint_encode_uint16(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, failure(EncodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::encode(int32_t value, Xi::ByteSpan dest) {
  auto reval =
      xi_encoding_varint_encode_int32(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, failure(EncodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::encode(uint32_t value, Xi::ByteSpan dest) {
  auto reval =
      xi_encoding_varint_encode_uint32(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, failure(EncodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::encode(int64_t value, Xi::ByteSpan dest) {
  auto reval =
      xi_encoding_varint_encode_int64(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, failure(EncodeError::OutOfMemory));
  return success(reval);
}

Xi::Result<size_t> Xi::Encoding::VarInt::encode(uint64_t value, Xi::ByteSpan dest) {
  auto reval =
      xi_encoding_varint_encode_uint64(value, dest.data(), static_cast<size_t>(dest.size()));
  XI_RETURN_EC_IF(reval == XI_VARINT_ENCODE_OUT_OF_MEMORY, failure(EncodeError::OutOfMemory));
  return success(reval);
}

bool Xi::Encoding::VarInt::hasSuccessor(Xi::Byte current) {
  return xi_encoding_varint_has_successor(current);
}
