/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <limits>
#include <cinttypes>

#include <Xi/Byte.hh>

namespace Xi {
namespace Database {

template <typename _EntityT, size_t _FixedSize = std::numeric_limits<uint32_t>::max()>
struct EntityTrait {
  static inline constexpr size_t fixedSize() {
    return _FixedSize;
  }
  static inline constexpr bool isFixedSize() {
    return fixedSize() < std::numeric_limits<uint32_t>::max();
  }

  using entity_type = _EntityT;
};

template <> struct EntityTrait<void> {
  static inline constexpr size_t fixedSize() {
    return 0;
  }
  static inline constexpr bool isFixedSize() {
    return true;
  }

  using entity_type = void;
};

}  // namespace Database
}  // namespace Xi

#define XI_DATABASE_ENTITY(TYPE)
#define XI_DATABASE_ENTITY_BLOB(TYPE)
