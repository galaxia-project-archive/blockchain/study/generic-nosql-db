/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>

#include "Xi/Database/ITable.hpp"
#include "Xi/Database/TableLock.hpp"

namespace Xi {
namespace Database {

XI_DECLARE_SMART_POINTER_CLASS(IDatabase)

/*!
 *
 * \brief The TableHandle class encapsulates a table resource usage without taking over its
 * lifetime.
 *
 * The database somehow needs to keep track who is currently locking/using a table, otherwise it may
 * release resources currently used.
 *
 * Another problem is a restart of the database while some other module keeps a table reference.
 * Therefore the Database itself keeps track of handles created and ensure their reference to tables
 * get resotred after the restart.
 *
 * \note During a restart table queries may fault.
 *
 * \attention The implementation uses an optimistic approach not enforcing timeouts, if you do not
 * trust that your implementation progresses while holding the lock you may stall a restart.
 *
 */
class TableHandle {
 private:
  explicit TableHandle(WeakITable table, WeakIDatabase database);
  friend IDatabase;

 public:
  /*!
   * \brief lock locks the handle to be used.
   * \return A nullptr if the table is unavailable otherwise a locked reference to the table.
   */
  TableLock lock();

 private:
  WeakITable m_table;
  WeakIDatabase m_database;
  TablePrefix m_prefix;
};

}  // namespace Database
}  // namespace Xi
