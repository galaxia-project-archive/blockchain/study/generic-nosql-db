/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <utility>

#include <Xi/Global.hh>
#include <Xi/Stream/InMemoryStreams.hpp>
#include <Xi/Serialization/Serializer.hpp>

#include "Xi/Database/TableTrait.hpp"
#include "Xi/Database/TableHandle.hpp"
#include "Xi/Database/Iterator.hpp"

namespace Xi {
namespace Database {

template <typename _TableTraitT> class Table;

template <typename _TableTraitT> using SharedTable = std::shared_ptr<Table<_TableTraitT>>;

template <typename _TableTraitT> class Table final {
 public:
  using Schema = TableSchema<_TableTraitT>;

 private:
  TableHandle m_handle;

 private:
  explicit Table(TableHandle handle) : m_handle{std::move(handle)} {
  }
  friend class IDatabase;

  TableLock lock() {
    return this->m_handle.lock();
  }

 public:
  [[nodiscard]] QueryError get(TableLock& handle, const typename Schema::key_type& key,
                               typename Schema::value_type& value) {
    typename Schema::key_storage keyBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeKey(handle, key, keyBuffer),
                        QueryError::SerializationError);
    typename Schema::value_storage valueBuffer;
    {
      auto valueStream = Stream::asOutputStream(valueBuffer);
      XI_RETURN_EC_IF_NOT(valueStream, QueryError::SerializationError);
      if (auto ec = handle->get(keyBuffer, *valueStream); ec != QueryError::Success) {
        return ec;
      }
    }
    XI_RETURN_EC_IF_NOT(Schema::deserializeValue(handle, valueBuffer, value),
                        QueryError::SerializationError);
    return QueryError::Success;
  }
  [[nodiscard]] QueryError get(const typename Schema::key_type& key,
                               typename Schema::value_type& value) {
    if (auto handle = this->lock()) {
      return this->get(handle, key, value);
    } else {
      return QueryError::Busy;
    }
  }
  [[nodiscard]] Result<typename Schema::value_type> get(const typename Schema::key_type& key) {
    typename Schema::value_type value;
    if (auto ec = this->get(key, value); ec != QueryError::Success) {
      return failure(ec);
    } else {
      return success(std::move(value));
    }
  }

  [[nodiscard]] QueryError put(TableLock& handle, const typename Schema::key_type& key,
                               const typename Schema::value_type& value) {
    typename Schema::key_storage keyBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeKey(handle, key, keyBuffer),
                        QueryError::SerializationError);
    typename Schema::value_storage valueBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeValue(handle, value, valueBuffer),
                        QueryError::SerializationError);
    return handle->put(keyBuffer, valueBuffer);
  }

  [[nodiscard]] QueryError put(const typename Schema::key_type& key,
                               const typename Schema::value_type& value) {
    if (auto handle = this->lock()) {
      return this->put(handle, key, value);
    } else {
      return QueryError::Busy;
    }
  }

  [[nodiscard]] QueryError put(const typename Schema::pair_type& pair) {
    return put(pair.first, pair.second);
  }

  [[nodiscard]] QueryError del(TableLock& handle, const typename Schema::key_type& key) {
    typename Schema::key_storage keyBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeKey(handle, key, keyBuffer),
                        QueryError::SerializationError);
    return handle->del(keyBuffer);
  }

  [[nodiscard]] QueryError del(const typename Schema::key_type& key) {
    if (auto handle = this->lock()) {
      return this->del(handle, key);
    } else {
      return QueryError::Busy;
    }
  }

  [[nodiscard]] QueryError del(TableLock& handle, const typename Schema::key_type& beginKey,
                               const typename Schema::key_type& exclusiveEndKey) {
    typename Schema::key_storage beginKeyBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeKey(handle, beginKey, beginKeyBuffer),
                        QueryError::SerializationError);
    typename Schema::key_storage exclusiveEndKeyBuffer;
    XI_RETURN_EC_IF_NOT(Schema::serializeKey(handle, exclusiveEndKey, exclusiveEndKeyBuffer),
                        QueryError::SerializationError);
    return handle->del(beginKeyBuffer, exclusiveEndKeyBuffer);
  }

  [[nodiscard]] QueryError del(const typename Schema::key_type& beginKey,
                               const typename Schema::key_type& exclusiveEndKey) {
    if (auto handle = this->lock()) {
      return this->del(handle, beginKey, exclusiveEndKey);
    } else {
      return QueryError::Busy;
    }
  }

  [[nodiscard]] std::unique_ptr<Iterator<_TableTraitT>> begin() {
    if (auto handle = this->lock()) {
      const auto& pref = handle->prefix();
      auto iterator = handle->range(pref.raw());
      if (iterator.get() != nullptr) {
        return std::make_unique<Iterator<_TableTraitT>>(std::move(iterator), std::move(handle));
      } else {
        return nullptr;
      }
    } else {
      return nullptr;
    }
  }

  [[nodiscard]] std::unique_ptr<Iterator<_TableTraitT>> end() {
    if (auto handle = this->lock()) {
      TablePrefix pref{handle->prefix().id() + 1U};
      auto iterator = handle->range(pref.raw());
      if (iterator.get() != nullptr) {
        return std::make_unique<Iterator<_TableTraitT>>(std::move(iterator), std::move(handle));
      } else {
        return nullptr;
      }
    } else {
      return nullptr;
    }
  }

  [[nodiscard]] std::unique_ptr<Iterator<_TableTraitT>> begin(
      const typename Schema::key_type offset) {
    auto iterator = this->begin();
    XI_RETURN_EC_IF(iterator.get() == nullptr, nullptr);
    XI_RETURN_EC_IF_NOT(iterator->seek(offset) == QueryError::Success, nullptr);
    return iterator;
  }
  [[nodiscard]] std::unique_ptr<Iterator<_TableTraitT>> end(
      const typename Schema::key_type offset) {
    return this->begin(offset);
  }
};

}  // namespace Database
}  // namespace Xi
