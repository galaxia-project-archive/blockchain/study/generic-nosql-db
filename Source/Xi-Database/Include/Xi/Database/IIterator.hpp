/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Database/QueryError.hpp"
#include "Xi/Database/TablePrefix.hpp"

namespace Xi {
namespace Database {

class IIterator {
 public:
  virtual ~IIterator() = default;

  [[nodiscard]] virtual bool isEnd() const = 0;
  [[nodiscard]] virtual QueryError error() const = 0;
  virtual void advance() = 0;
  virtual void seek(ConstByteSpan offset) = 0;
  virtual TablePrefix prefix() const = 0;
  virtual ConstByteSpan key() const = 0;
  virtual ConstByteSpan value() const = 0;

  inline operator bool() {
    return !isEnd();
  }
  inline ConstByteSpan operator*() const {
    return value();
  }
};

XI_DECLARE_SMART_POINTER(IIterator)

}  // namespace Database
}  // namespace Xi
