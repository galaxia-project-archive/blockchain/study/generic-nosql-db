/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Database/TablePrefix.hpp"

#include <Xi/Endianess/Endianess.hh>

Xi::Database::TablePrefix::TablePrefix() : m_id{0} {
}

Xi::Database::TablePrefix::TablePrefix(id_type id) : m_id{id} {
}

Xi::Database::TablePrefix::id_type Xi::Database::TablePrefix::id() const {
  return m_id;
}

Xi::ConstByteSpan Xi::Database::TablePrefix::raw() const {
  return asConstByteSpan(&m_id, bytes());
}

bool Xi::Database::TablePrefix::operator==(const Xi::Database::TablePrefix &rhs) const {
  return id() == rhs.id();
}

bool Xi::Database::TablePrefix::operator!=(const Xi::Database::TablePrefix &rhs) const {
  return id() != rhs.id();
}

std::size_t std::hash<Xi::Database::TablePrefix>::operator()(
    const Xi::Database::TablePrefix &prefix) const {
  return std::hash<uint16_t>{}(prefix.id());
}
