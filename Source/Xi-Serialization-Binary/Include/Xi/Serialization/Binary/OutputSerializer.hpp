/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <stack>
#include <utility>

#include <Xi/Stream/IOutputStream.hpp>
#include <Xi/Serialization/IOutputSerializer.hpp>

#include "Xi/Serialization/Binary/Options.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {
class OutputSerializer final : public IOutputSerializer {
 public:
  OutputSerializer(Stream::IOutputStream &stream, Options options = Options{});
  ~OutputSerializer() override = default;

  [[nodiscard]] bool writeInt8(std::int8_t value, const std::string_view name) override;
  [[nodiscard]] bool writeUInt8(std::uint8_t value, const std::string_view name) override;
  [[nodiscard]] bool writeInt16(std::int16_t value, const std::string_view name) override;
  [[nodiscard]] bool writeUInt16(std::uint16_t value, const std::string_view name) override;
  [[nodiscard]] bool writeInt32(std::int32_t value, const std::string_view name) override;
  [[nodiscard]] bool writeUInt32(std::uint32_t value, const std::string_view name) override;
  [[nodiscard]] bool writeInt64(std::int64_t value, const std::string_view name) override;
  [[nodiscard]] bool writeUInt64(std::uint64_t value, const std::string_view name) override;

  [[nodiscard]] bool writeBoolean(bool value, const std::string_view name) override;

  [[nodiscard]] bool writeFloat(float value, const std::string_view name) override;
  [[nodiscard]] bool writeDouble(double value, const std::string_view name) override;

  [[nodiscard]] bool writeTypeTag(const TypeTag &value, const std::string_view name) override;
  [[nodiscard]] bool writeFlag(const TypeTagVector &flag, const std::string_view name) override;

  [[nodiscard]] bool writeString(const std::string_view value,
                                 const std::string_view name) override;
  [[nodiscard]] bool writeBinary(ConstByteSpan value, const std::string_view name) override;

  [[nodiscard]] bool writeBlob(ConstByteSpan value, const std::string_view name) override;

  [[nodiscard]] bool beginWriteComplex(const std::string_view name) override;
  [[nodiscard]] bool endWriteComplex() override;

  [[nodiscard]] bool beginWriteVector(size_t size, const std::string_view name) override;
  [[nodiscard]] bool endWriteVector() override;

  [[nodiscard]] bool beginWriteArray(size_t size, const std::string_view name) override;
  [[nodiscard]] bool endWriteArray() override;

  [[nodiscard]] bool writeNull(const std::string_view name) override;
  [[nodiscard]] bool writeNotNull(const std::string_view name) override;

 private:
  Stream::IOutputStream &m_stream;
  Options m_options;
};
}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi
