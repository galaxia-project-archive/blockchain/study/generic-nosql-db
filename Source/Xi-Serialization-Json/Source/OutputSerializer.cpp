/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Json/OutputSerializer.hpp"

#include <utility>
#include <numeric>

#include <Xi/ExternalIncludePush.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <Xi/ExternalIncludePop.h>

#include <Xi/Exceptions.hpp>
#include <Xi/Encoding/Hex.hpp>
#include <Xi/Stream/InMemoryStreams.hpp>

namespace {
struct JOutputStreamWrapper {
  using Ch = char;

  Xi::Stream::IOutputStream &stream;

  JOutputStreamWrapper(Xi::Stream::IOutputStream &_stream) : stream{_stream} {
  }
  void Put(Ch ch) {
    XI_UNUSED_REVAL(
        Xi::Stream::writeStrict(stream, {reinterpret_cast<Xi::Byte *>(&ch), sizeof(Ch)}));
  }
  void Flush() {
    XI_UNUSED_REVAL(stream.flush());
  }

  [[noreturn]] Ch Peek() const {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] Ch Take() {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] size_t Tell() {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] Ch *PutBegin() {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] size_t PutEnd(Ch *) {
    Xi::exceptional<Xi::NotImplementedError>();
  }
};
}  // namespace

struct Xi::Serialization::Json::OutputSerializer::_Impl {
  JOutputStreamWrapper stream;
  rapidjson::Writer<JOutputStreamWrapper> writer;

  [[nodiscard]] bool emplaceKey(const std::string_view name) {
    exceptional_if<InvalidSizeError>(name.size() > std::numeric_limits<rapidjson::SizeType>::max());
    if (!name.empty()) {
      XI_RETURN_EC_IF_NOT(
          writer.Key(name.data(), static_cast<rapidjson::SizeType>(name.size()), true), false);
      return true;
    } else {
      return true;
    }
  }

  _Impl(Stream::IOutputStream &native) : stream{native}, writer{stream} {
  }
};

using JValue = rapidjson::Value;

Xi::Serialization::Json::OutputSerializer::OutputSerializer(Stream::IOutputStream &stream)
    : m_impl{new _Impl{stream}} {
}

Xi::Serialization::Json::OutputSerializer::~OutputSerializer() {
}

bool Xi::Serialization::Json::OutputSerializer::writeInt8(int8_t value,
                                                          const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Int(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeUInt8(uint8_t value,
                                                           const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Uint(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeInt16(int16_t value,
                                                           const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Int(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeUInt16(uint16_t value,
                                                            const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Uint(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeInt32(int32_t value,
                                                           const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Int(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeUInt32(uint32_t value,
                                                            const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Uint(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeInt64(int64_t value,
                                                           const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Int64(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeUInt64(uint64_t value,
                                                            const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Uint64(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeBoolean(bool value,
                                                             const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Bool(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeFloat(float value,
                                                           const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Double(static_cast<double>(value)), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeDouble(double value,
                                                            const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Double(value), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeTypeTag(
    const Xi::Serialization::TypeTag &value, const std::string_view name) {
  XI_RETURN_EC_IF(value.text() == TypeTag::NoTextTag, false);
  return writeString(value.text(), name);
}

bool Xi::Serialization::Json::OutputSerializer::writeFlag(
    const Xi::Serialization::TypeTagVector &flag, const std::string_view name) {
  XI_RETURN_EC_IF(flag.size() > TypeTag::maximumFlags(), false);
  uint16_t nativeFlag = 0;
  for (const auto &iFlag : flag) {
    XI_RETURN_EC_IF(iFlag.binary() == TypeTag::NoBinaryTag, false);
    XI_RETURN_EC_IF(iFlag.binary() > 15, false);
    assert(iFlag.binary() > 0);
    nativeFlag |= (1 << (iFlag.binary() - 1));
  }
  return writeUInt16(nativeFlag, name);
}

bool Xi::Serialization::Json::OutputSerializer::writeString(const std::string_view value,
                                                            const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF(name.size() > std::numeric_limits<rapidjson::SizeType>::max(), false);
  XI_RETURN_EC_IF(value.size() > std::numeric_limits<rapidjson::SizeType>::max(), false);
  XI_RETURN_EC_IF_NOT(
      m_impl->writer.String(value.data(), static_cast<rapidjson::SizeType>(value.size())), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeBinary(ConstByteSpan value,
                                                            const std::string_view name) {
  std::string hex;
  Stream::InMemoryInputStream inStream{value};
  Stream::StringOutputStream outStream{hex};
  XI_RETURN_EC_IF_NOT(Encoding::Hex::encode(inStream, outStream), false);
  return writeString(hex, name);
}

bool Xi::Serialization::Json::OutputSerializer::writeBlob(Xi::ConstByteSpan value,
                                                          const std::string_view name) {
  return writeBinary(value, name);
}

bool Xi::Serialization::Json::OutputSerializer::beginWriteComplex(const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.StartObject(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::endWriteComplex() {
  XI_RETURN_EC_IF_NOT(m_impl->writer.EndObject(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::beginWriteVector(size_t,
                                                                 const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.StartArray(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::endWriteVector() {
  XI_RETURN_EC_IF_NOT(m_impl->writer.EndArray(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::beginWriteArray(size_t,
                                                                const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.StartArray(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::endWriteArray() {
  XI_RETURN_EC_IF_NOT(m_impl->writer.EndArray(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeNull(const std::string_view name) {
  XI_RETURN_EC_IF_NOT(m_impl->emplaceKey(name), false);
  XI_RETURN_EC_IF_NOT(m_impl->writer.Null(), false);
  return true;
}

bool Xi::Serialization::Json::OutputSerializer::writeNotNull(const std::string_view) {
  return true;
}
