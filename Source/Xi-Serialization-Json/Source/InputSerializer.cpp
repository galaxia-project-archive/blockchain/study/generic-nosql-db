/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Serialization/Json/InputSerializer.hpp"

#include <numeric>
#include <utility>
#include <stack>
#include <unordered_set>

#include <Xi/ExternalIncludePush.h>
#include <rapidjson/document.h>
#include <Xi/ExternalIncludePop.h>

#include <Xi/Exceptions.hpp>
#include <Xi/Encoding/Base64.hpp>
#include <Xi/Stream/InMemoryStreams.hpp>

namespace {
struct JInputStreamWrapper {
  using Ch = char;

  Xi::Stream::IInputStream &stream;

  JInputStreamWrapper(Xi::Stream::IInputStream &_stream) : stream{_stream} {
  }

  Ch Peek() const {
    if (stream.isEndOfStream()) {
      return '\0';
    } else {
      const auto byte = stream.peek();
      return *reinterpret_cast<const char *>(&byte);
    }
  }
  Ch Take() {
    if (stream.isEndOfStream()) {
      return '\0';
    } else {
      const auto byte = stream.take();
      return *reinterpret_cast<const char *>(&byte);
    }
  }
  size_t Tell() {
    return stream.tell();
  }
  Ch *PutBegin() {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] void Put(Ch) {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] void Flush() {
    Xi::exceptional<Xi::NotImplementedError>();
  }
  [[noreturn]] size_t PutEnd(Ch *) {
    Xi::exceptional<Xi::NotImplementedError>();
  }
};
}  // namespace

struct Xi::Serialization::Json::InputSerializer::_Impl {
  rapidjson::Document document;
  /// (value x arrayIndex))
  std::stack<std::pair<rapidjson::Value, size_t>> valueStack;

  _Impl(Stream::IInputStream &stream) {
    JInputStreamWrapper is{stream};
    document.ParseStream(is);
  }

  [[nodiscard]] bool readGenericMember(rapidjson::Value &out, const std::string_view name) {
    XI_RETURN_EC_IF(valueStack.empty(), false);
    auto &top = valueStack.top();
    if (name.empty()) {
      XI_RETURN_EC_IF_NOT(top.first.IsArray(), false);
      const auto index = top.second++;
      XI_RETURN_EC_IF_NOT(index < top.first.Size(), false);
      out = top.first[static_cast<rapidjson::SizeType>(index)];
      return true;
    } else {
      XI_RETURN_EC_IF_NOT(top.first.IsObject(), false);
      auto search = top.first.FindMember(rapidjson::Value{
          name.data(), static_cast<rapidjson::SizeType>(name.size()), document.GetAllocator()});
      XI_RETURN_EC_IF(search == top.first.MemberEnd(), false);
      out = search->value;
      return true;
    }
  }
};

Xi::Serialization::Json::InputSerializer::InputSerializer(Xi::Stream::IInputStream &stream)
    : m_impl{new _Impl{stream}} {
}

Xi::Serialization::Json::InputSerializer::~InputSerializer() {
}

bool Xi::Serialization::Json::InputSerializer::readInt8(std::int8_t &value,
                                                        const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsInt(), false);
  auto reval = token.GetInt();
  XI_RETURN_EC_IF(reval > std::numeric_limits<int8_t>::max(), false);
  XI_RETURN_EC_IF(reval < std::numeric_limits<int8_t>::min(), false);
  value = static_cast<int8_t>(reval);
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readUInt8(std::uint8_t &value,
                                                         const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsUint(), false);
  auto reval = token.GetUint();
  XI_RETURN_EC_IF(reval > std::numeric_limits<uint8_t>::max(), false);
  value = static_cast<uint8_t>(reval);
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readInt16(std::int16_t &value,
                                                         const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsInt(), false);
  auto reval = token.GetInt();
  XI_RETURN_EC_IF(reval > std::numeric_limits<int16_t>::max(), false);
  XI_RETURN_EC_IF(reval < std::numeric_limits<int16_t>::min(), false);
  value = static_cast<int16_t>(reval);
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readUInt16(std::uint16_t &value,
                                                          const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsUint(), false);
  auto reval = token.GetUint();
  XI_RETURN_EC_IF(reval > std::numeric_limits<uint16_t>::max(), false);
  value = static_cast<uint16_t>(reval);
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readInt32(std::int32_t &value,
                                                         const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsInt(), false);
  value = token.GetInt();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readUInt32(std::uint32_t &value,
                                                          const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsUint(), false);
  value = token.GetUint();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readInt64(std::int64_t &value,
                                                         const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsInt64(), false);
  value = token.GetInt64();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readUInt64(std::uint64_t &value,
                                                          const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsUint64(), false);
  value = token.GetUint64();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readBoolean(bool &value,
                                                           const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsBool(), false);
  value = token.GetBool();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readFloat(float &value,
                                                         const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsFloat(), false);
  value = token.GetFloat();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readDouble(double &value,
                                                          const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsDouble(), false);
  value = token.GetDouble();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readTypeTag(TypeTag &tag,
                                                           const std::string_view name) {
  std::string stringTag = TypeTag::NoTextTag;
  XI_RETURN_EC_IF_NOT(readString(stringTag, name), false);
  XI_RETURN_EC_IF(stringTag == TypeTag::NoTextTag, false);
  tag = TypeTag{TypeTag::NoBinaryTag, std::move(stringTag)};
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readFlag(Xi::Serialization::TypeTagVector &value,
                                                        const std::string_view name) {
  bool hasFlag = true;
  XI_RETURN_EC_IF_NOT(checkNull(hasFlag, name), false);
  XI_RETURN_EC_IF_NOT(hasFlag, true);

  size_t size = 0;
  XI_RETURN_EC_IF_NOT(beginReadArray(size, name), false);
  XI_RETURN_EC_IF(size > TypeTag::maximumFlags(), false);

  std::unordered_set<std::string> processedFlags{};
  for (size_t i = 0; i < size; ++i) {
    TypeTag iTag = TypeTag::Null;
    XI_RETURN_EC_IF_NOT(readTypeTag(iTag, ""), false);
    XI_RETURN_EC_IF(iTag.text() == TypeTag::NoTextTag, false);
    XI_RETURN_EC_IF_NOT(processedFlags.insert(iTag.text()).second, false);
    value.push_back(iTag);
  }
  return endReadArray();
}

bool Xi::Serialization::Json::InputSerializer::readString(std::string &out,
                                                          const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsString(), false);
  out = std::string{token.GetString(), token.GetStringLength()};
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readBinary(ByteVector &out,
                                                          const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsString(), false);
  out.clear();
  Stream::InMemoryInputStream iStream{
      ConstByteSpan{reinterpret_cast<const Byte *>(token.GetString()), token.GetStringLength()}};
  Stream::ByteVectorOutputStream oStream{out};
  XI_RETURN_EC_IF_NOT(Encoding::Base64::decode(iStream, oStream), false);
  return true;
}

bool Xi::Serialization::Json::InputSerializer::readBlob(ByteSpan out, const std::string_view name) {
  rapidjson::Value token;
  XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
  XI_RETURN_EC_IF_NOT(token.IsString(), false);
  Stream::InMemoryInputStream iStream{
      ConstByteSpan{reinterpret_cast<const Byte *>(token.GetString()), token.GetStringLength()}};
  Stream::ByteArrayOutputStream oStream{out};
  XI_RETURN_EC_IF_NOT(Encoding::Base64::decode(iStream, oStream), false);
  return oStream.written().size() == out.size();
}

bool Xi::Serialization::Json::InputSerializer::beginReadComplex(const std::string_view name) {
  if (m_impl->valueStack.empty()) {
    XI_RETURN_EC_IF_NOT(m_impl->document.IsObject(), false);
    m_impl->valueStack.emplace(m_impl->document.GetObject(), 0);
    return true;
  } else {
    rapidjson::Value token;
    XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
    XI_RETURN_EC_IF_NOT(token.IsObject(), false);
    m_impl->valueStack.emplace(std::move(token), 0);
    return true;
  }
}

bool Xi::Serialization::Json::InputSerializer::endReadComplex() {
  XI_RETURN_EC_IF(m_impl->valueStack.empty(), false);
  XI_RETURN_EC_IF_NOT(m_impl->valueStack.top().first.IsObject(), false);
  m_impl->valueStack.pop();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::beginReadVector(size_t &size,
                                                               const std::string_view name) {
  if (m_impl->valueStack.empty()) {
    XI_RETURN_EC_IF_NOT(m_impl->document.IsArray(), false);
    m_impl->valueStack.emplace(m_impl->document.GetArray(), 0);
    size = m_impl->valueStack.top().first.Size();
    return true;
  } else {
    rapidjson::Value token;
    XI_RETURN_EC_IF_NOT(m_impl->readGenericMember(token, name), false);
    XI_RETURN_EC_IF_NOT(token.IsArray(), false);
    size = token.Size();
    m_impl->valueStack.emplace(std::move(token), 0);
    return true;
  }
}

bool Xi::Serialization::Json::InputSerializer::endReadVector() {
  XI_RETURN_EC_IF(m_impl->valueStack.empty(), false);
  XI_RETURN_EC_IF_NOT(m_impl->valueStack.top().first.IsArray(), false);
  m_impl->valueStack.pop();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::beginReadArray(size_t size,
                                                              const std::string_view name) {
  size_t actualSize = ~size;
  XI_RETURN_EC_IF_NOT(beginReadVector(actualSize, name), false);
  return actualSize == size;
}

bool Xi::Serialization::Json::InputSerializer::endReadArray() {
  XI_RETURN_EC_IF(m_impl->valueStack.empty(), false);
  XI_RETURN_EC_IF_NOT(m_impl->valueStack.top().first.IsArray(), false);
  m_impl->valueStack.pop();
  return true;
}

bool Xi::Serialization::Json::InputSerializer::checkNull(bool &isNull,
                                                         const std::string_view name) {
  XI_RETURN_EC_IF(m_impl->valueStack.empty(), false);
  auto &top = m_impl->valueStack.top();
  if (name.empty()) {
    XI_RETURN_EC_IF_NOT(top.first.IsArray(), false);
    const auto index = top.second++;
    if (index >= top.first.Size()) {
      isNull = false;
      return true;
    } else {
      isNull = top.first[static_cast<rapidjson::SizeType>(index)].IsNull();
      return true;
    }
  } else {
    XI_RETURN_EC_IF_NOT(top.first.IsObject(), false);
    auto search = top.first.FindMember(
        rapidjson::Value{name.data(), static_cast<rapidjson::SizeType>(name.size()),
                         m_impl->document.GetAllocator()});
    if (search == top.first.MemberEnd()) {
      isNull = true;
      return true;
    } else {
      isNull = search->value.IsNull();
      return true;
    }
  }
}
