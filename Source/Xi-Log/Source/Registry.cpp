/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Registry.hpp"

#include <memory>
#include <utility>
#include <mutex>
#include <iostream>

#include <Xi/Stream/OStream.hpp>
#include <Xi/Algorithm/String.h>

#include "Xi/Log/Sink.hpp"
#include "Xi/Log/Handle.hpp"
#include "Xi/Log/Registry.hpp"
#include "Xi/Log/Filter.hpp"

class Xi::Log::Registry::LoggerWrapper final : public Xi::Log::ILogger {
 public:
  SharedILogger logger;

  bool isFiltered(const Level level) const override {
    if (logger) {
      return logger->isFiltered(level);
    } else {
      return true;
    }
  }

  void print(Context context, std::string_view message) override {
    if (logger) {
      logger->print(std::move(context), message);
    }
  }

  void swap(SharedILogger newLogger) {
    logger = newLogger;  // TODO: CAS
  }
};

Xi::Log::Registry::Registry()
    : m_categories{{"None", SharedCategory{new Category{"None", "None"}}}},
      m_logger{std::make_shared<LoggerWrapper>()},
      m_guard{} {
  doPut(std::make_shared<Filter>(
      std::make_shared<Sink>(std::make_unique<Xi::Stream::OStream>(std::cout))));
}

Xi::Log::Registry& Xi::Log::Registry::singleton() {
  static std::unique_ptr<Registry> __Singleton{new Registry};
  return *__Singleton;
}

Xi::Log::Registry::~Registry() {
}

Xi::Log::Handle Xi::Log::Registry::get(std::string_view category) {
  return Registry::singleton().doGet(category);
}

void Xi::Log::Registry::put(Xi::Log::SharedILogger logger) {
  Registry::singleton().doPut(logger);
}

Xi::Log::Handle Xi::Log::Registry::doGet(std::string_view category) {
  XI_CONCURRENT_RLOCK(m_guard);

  auto path = split(category, ":./");
  if (path.empty()) {
    path.push_back("None");
  }
  auto node = getCategory(path.front(), "");
  for (size_t i = 1; i < path.size(); ++i) {
    node = getCategory(path[i], node->fullName());
  }
  return Handle{node, m_logger};
}

void Xi::Log::Registry::doPut(Xi::Log::SharedILogger logger) {
  m_logger->swap(logger);
}

Xi::Log::SharedCategory Xi::Log::Registry::getCategory(const std::string& category,
                                                       const std::string& path) {
  XI_CONCURRENT_RLOCK(m_guard);

  std::string fullPath = path;
  if (!fullPath.empty()) {
    fullPath += ".";
  }
  fullPath += category;
  if (auto search = m_categories[fullPath]) {
    return search;
  } else {
    return m_categories[fullPath] = SharedCategory{new Category{category, fullPath}};
  }
}
