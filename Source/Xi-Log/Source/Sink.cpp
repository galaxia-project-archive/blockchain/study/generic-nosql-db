/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Sink.hpp"

#include <utility>

#include <Xi/Exceptions.hpp>
#include <Xi/Stream/InMemoryStreams.hpp>

Xi::Log::Sink::Sink(Stream::UniqueIOutputStream stream) : m_sink{std::move(stream)} {
  exceptional_if<NullArgumentError>(m_sink.get() == nullptr,
                                    "log sink expects a none null output stream");
}

Xi::Log::Sink::~Sink() {
  (void)m_sink->flush();
}

bool Xi::Log::Sink::isFiltered(const Xi::Log::Level level) const {
  XI_UNUSED(level);
  return false;
}

void Xi::Log::Sink::print(Xi::Log::Context context, std::string_view message) {
  XI_UNUSED(context);
  (void)Stream::writeStrict(*m_sink, asByteSpan(message));
  (void)m_sink->write(asByteSpan(std::string_view{"\n"}));
}
