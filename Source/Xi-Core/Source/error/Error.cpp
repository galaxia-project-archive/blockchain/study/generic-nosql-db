﻿/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Error/Error.hpp"

namespace Xi {
namespace Error {
Error::Error() : m_errorCode{make_error_code(GlitchError::NotInitialized)} {
  /* */
}

Error::Error(const Error& other) : m_errorCode{other.m_errorCode} {
  /* */
}

Error& Error::operator=(const Error& other) {
  m_errorCode = other.m_errorCode;
  return *this;
}

Error::Error(Error&& other) : m_errorCode{other.m_errorCode} {
  other.m_errorCode = make_error_code(GlitchError::Moved);
}

Error& Error::operator=(Error&& other) {
  m_errorCode = other.m_errorCode;
  other.m_errorCode = make_error_code(GlitchError::Moved);
  return *this;
}
}  // namespace Error
}  // namespace Xi
