/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <system_error>

#include "Xi/Error/Code.hpp"

namespace Xi {
namespace Error {

/*!
 *
 * \brief A GlitchError is an unexpected error occuring on an error itself.
 *
 * Normally those errors should not be engaged, as this is more a missuse of the api rather than
 * an expected error that can occur. However returning a well defined error state seems better
 * than running into undefined behaviour or throwing an exception.
 *
 */
XI_ERROR_CODE_BEGIN(Glitch)
/// Error was allocated but actually not initialized with a specific error code.
XI_ERROR_CODE_VALUE(NotInitialized, 0x0001)
/// Error code was already moved and should not be used anymore.
XI_ERROR_CODE_VALUE(Moved, 0x0002)
XI_ERROR_CODE_END(Glitch, "")

}  // namespace Error
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Error, Glitch)
