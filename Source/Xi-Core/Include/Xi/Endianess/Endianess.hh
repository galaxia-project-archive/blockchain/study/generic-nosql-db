/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Endianess/Identity.hh"
#include "Xi/Endianess/Swap.hh"
#include "Xi/Endianess/Type.hh"
#include "Xi/Endianess/Big.hh"
#include "Xi/Endianess/Little.hh"

#if defined(__cplusplus)

namespace Xi {
namespace Endianess {

[[nodiscard]] static inline constexpr uint16_t convert(uint16_t value, const Type target) {
  switch (target) {
    case Type::Native:
      return identity(value);
    case Type::Little:
      return little(value);
    case Type::Big:
      return big(value);
  }
  return identity(value);
}
[[nodiscard]] static inline constexpr uint32_t convert(uint32_t value, const Type target) {
  switch (target) {
    case Type::Native:
      return identity(value);
    case Type::Little:
      return little(value);
    case Type::Big:
      return big(value);
  }
  return identity(value);
}
[[nodiscard]] static inline constexpr uint64_t convert(uint64_t value, const Type target) {
  switch (target) {
    case Type::Native:
      return identity(value);
    case Type::Little:
      return little(value);
    case Type::Big:
      return big(value);
  }
  return identity(value);
}
[[nodiscard]] static inline constexpr int16_t convert(int16_t value, const Type target) {
  switch (target) {
    case Type::Native:
      return identity(value);
    case Type::Little:
      return little(value);
    case Type::Big:
      return big(value);
  }
  return identity(value);
}
[[nodiscard]] static inline constexpr int32_t convert(int32_t value, const Type target) {
  switch (target) {
    case Type::Native:
      return identity(value);
    case Type::Little:
      return little(value);
    case Type::Big:
      return big(value);
  }
  return identity(value);
}
[[nodiscard]] static inline constexpr int64_t convert(int64_t value, const Type target) {
  switch (target) {
    case Type::Native:
      return identity(value);
    case Type::Little:
      return little(value);
    case Type::Big:
      return big(value);
  }
  return identity(value);
}

}  // namespace Endianess
}  // namespace Xi

#endif
