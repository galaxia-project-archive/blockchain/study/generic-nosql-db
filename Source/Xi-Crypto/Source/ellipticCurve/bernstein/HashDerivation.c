/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "ellipticCurve/bernstein/HashDerivation.hh"

#include <Xi/Global.hh>

#include "Xi/Crypto/EllipticCurve/Hash.hh"

void ge_p3_from_hash(ge_p3* out, const xi_byte_t* hash) {
  ge_p2 point;
  ge_p1p1 point2;
  ge_fromfe_frombytes_vartime(&point, hash);
  ge_mul8(&point2, &point);
  ge_p1p1_to_p3(out, &point2);
}

int ge_p3_from_message(ge_p3* out, const xi_byte_t* message, size_t length) {
  xi_byte_t messageHash[XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE];
  const int ec = xi_crypto_elliptic_curve_hash(message, length, messageHash);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, ec);
  ge_p3_from_hash(out, messageHash);
  return XI_RETURN_CODE_SUCCESS;
}
