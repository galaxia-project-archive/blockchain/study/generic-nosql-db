/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <string>
#include <unordered_set>
#include <map>

#include <Xi/Global.hh>
#include <Xi/Concurrent/ReadersWriterLock.h>
#include <Xi/Concurrent/RecursiveLock.h>
#include <Xi/Database/IDatabase.hpp>
#include <Xi/Database/ITable.hpp>

#include "Xi/Database/RocksDB/Options.hpp"

namespace rocksdb {
class DB;
}

namespace Xi {
namespace Database {
namespace RocksDB {

XI_DECLARE_SMART_POINTER_CLASS(Database)

class Database final : public IDatabase {
 public:
  static SharedDatabase create(Options options = Options{});

 private:
  explicit Database(Options options);
  XI_DELETE_COPY(Database);
  XI_DELETE_MOVE(Database);

 public:
  ~Database() override;

 protected:
  [[nodiscard]] bool doInitialize() override;
  [[nodiscard]] bool doShutdown() override;
  [[nodiscard]] UniqueITable doRequireTable(const TablePrefix& prefix) override;
  [[nodiscard]] bool doReleaseTable(ITable& table) override;

 private:
  Options m_options{};
  std::unique_ptr<rocksdb::DB> m_handle{nullptr};
};

}  // namespace RocksDB
}  // namespace Database
}  // namespace Xi
