/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>
#include <string>
#include <cinttypes>

#include <Xi/Serialization/Serialization.hpp>

namespace rocksdb {
struct Options;
}

namespace Xi {
namespace Database {
namespace RocksDB {

enum struct CacheStrategy { None = 1, LastRecentlyUsed, Clock };
XI_SERIALIZATION_ENUM(CacheStrategy)

struct Options {
  std::optional<std::string> dataDirectory = std::nullopt;
  std::optional<uint16_t> threads = std::nullopt;

  std::optional<uint64_t> writeBufferSize = std::nullopt;
  std::optional<CacheStrategy> cacheStrategy = std::nullopt;
  std::optional<size_t> cacheSize = std::nullopt;

  std::optional<size_t> targetFileSize = std::nullopt;
  std::optional<int32_t> targetFileSizeMultiplier = std::nullopt;

  std::optional<uint8_t> levels = std::nullopt;

  XI_SERIALIZATION_COMPLEX_BEGIN

  XI_SERIALIZATION_MEMBER(dataDirectory, "data_directory")
  XI_SERIALIZATION_MEMBER(threads, "threads")

  XI_SERIALIZATION_MEMBER(writeBufferSize, "write_buffer_size")
  XI_SERIALIZATION_MEMBER(cacheStrategy, "cache_strategy")
  XI_SERIALIZATION_MEMBER(cacheSize, "cache_size")

  XI_SERIALIZATION_MEMBER(targetFileSize, "target_file_size")
  XI_SERIALIZATION_MEMBER(targetFileSizeMultiplier, "target_file_size_multiplier")

  XI_SERIALIZATION_MEMBER(levels, "levels")

  XI_SERIALIZATION_COMPLEX_END

  rocksdb::Options toNative() const;
};

}  // namespace RocksDB
}  // namespace Database
}  // namespace Xi

XI_SERIALIZATION_ENUM_RANGE(Xi::Database::RocksDB::CacheStrategy, None, Clock)
XI_SERIALIZATION_ENUM_TAG(Xi::Database::RocksDB::CacheStrategy, None, "none")
XI_SERIALIZATION_ENUM_TAG(Xi::Database::RocksDB::CacheStrategy, LastRecentlyUsed,
                          "last_recently_used")
XI_SERIALIZATION_ENUM_TAG(Xi::Database::RocksDB::CacheStrategy, Clock, "clock")
