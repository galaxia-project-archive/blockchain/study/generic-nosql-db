/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <rocksdb/db.h>

#include <Xi/Database/ITable.hpp>
#include <Xi/Database/QueryError.hpp>

namespace Xi {
namespace Database {
namespace RocksDB {

QueryError toQueryError(const rocksdb::Status& status);

class Table final : public ITable {
 public:
  explicit Table(rocksdb::DB* db, const TablePrefix& prefix);
  ~Table() override;

  void release();

  [[nodiscard]] Serialization::UniqueIInputSerializer createKeyInputSerailizer(
      Stream::IInputStream& input) const override;
  [[nodiscard]] Serialization::UniqueIOutputSerializer createKeyOutputSerailizer(
      Stream::IOutputStream& output) const override;
  [[nodiscard]] Serialization::UniqueIInputSerializer createValueInputSerailizer(
      Stream::IInputStream& input) const override;
  [[nodiscard]] Serialization::UniqueIOutputSerializer createValueOutputSerailizer(
      Stream::IOutputStream& output) const override;

  [[nodiscard]] QueryError get(ConstByteSpan key, Stream::IOutputStream& valueOutput) override;
  [[nodiscard]] QueryError put(ConstByteSpan key, ConstByteSpan value) override;
  [[nodiscard]] QueryError del(ConstByteSpan key) override;
  [[nodiscard]] QueryError del(ConstByteSpan range, ConstByteSpan exclusiveEnd) override;
  [[nodiscard]] UniqueIIterator range(ConstByteSpan start) override;

 private:
  rocksdb::DB* m_db;
  rocksdb::WriteOptions m_writeOptions;
};

}  // namespace RocksDB
}  // namespace Database
}  // namespace Xi
