/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Logger.hpp"

#include <utility>
#include <cstdio>

#include <Xi/Log/Log.hpp>

Xi::Database::RocksDB::Logger::Logger() : m_logger{Log::Registry::get("Database/RocksDB/Native")} {
}

void Xi::Database::RocksDB::Logger::Logv(const char *format, va_list ap) {
  if (!m_logger.isFiltered(Log::Level::Debug)) {
    std::string formatted = formatString(format, ap);
    m_logger.log(Log::Level::Debug, formatted);
  }
}

void Xi::Database::RocksDB::Logger::Logv(const rocksdb::InfoLogLevel log_level, const char *format,
                                         va_list ap) {
  const auto level = nativeToLoggingLevel(log_level);
  if (!m_logger.isFiltered(level)) {
    std::string formatted = formatString(format, ap);
    m_logger.log(level, formatted);
  }
}

Xi::Log::Level Xi::Database::RocksDB::Logger::nativeToLoggingLevel(
    rocksdb::InfoLogLevel logLevel) const {
  switch (logLevel) {
    case rocksdb::DEBUG_LEVEL:
      return Xi::Log::Level::Debug;
    case rocksdb::INFO_LEVEL:
      return Xi::Log::Level::Info;
    case rocksdb::WARN_LEVEL:
      return Xi::Log::Level::Warn;
    case rocksdb::ERROR_LEVEL:
      return Xi::Log::Level::Error;
    case rocksdb::FATAL_LEVEL:
      return Xi::Log::Level::Fatal;
    case rocksdb::HEADER_LEVEL:
      return Xi::Log::Level::Info;
    case rocksdb::NUM_INFO_LOG_LEVELS:
      return Xi::Log::Level::Info;
  }
  return Xi::Log::Level::Trace;
}

std::string Xi::Database::RocksDB::Logger::formatString(const char *format, va_list ap) const {
  std::string reval{};
  reval.resize(128);
  int ec = vsnprintf(reval.data(), reval.size(), format, ap);
  if (ec < 0) {
    return std::string{""};
  } else {
    reval.resize(static_cast<size_t>(ec));
    return reval;
  }
}
