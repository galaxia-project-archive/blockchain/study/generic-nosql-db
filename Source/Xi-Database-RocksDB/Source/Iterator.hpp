/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>

#include <rocksdb/iterator.h>

#include <Xi/Database/IIterator.hpp>

namespace Xi {
namespace Database {
namespace RocksDB {

/*!
 *
 * \brief The Iterator class wraps the rocksdb iterator to provide abstract interaction of other
 * modules.
 *
 */
class Iterator final : public IIterator {
 public:
  using native_iterator = rocksdb::Iterator;

  /*!
   *
   * \brief Iterator constructs a new wrapper taking care of the memory lifetime.
   * \param iterator The iterator to wrap.
   * \exception NullArgumentError
   *
   * \attention The caller MUST check for a nullptr in advance, NEVER construct an iterator from a
   * nullptr.
   *
   */
  Iterator(native_iterator* iterator);

  [[nodiscard]] bool isEnd() const override;
  [[nodiscard]] QueryError error() const override;
  void advance() override;
  void seek(ConstByteSpan offset) override;
  TablePrefix prefix() const override;
  ConstByteSpan key() const override;
  ConstByteSpan value() const override;

 private:
  std::unique_ptr<native_iterator> m_iterator;
};

}  // namespace RocksDB
}  // namespace Database
}  // namespace Xi
