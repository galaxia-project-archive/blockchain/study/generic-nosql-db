/* ==============================================================================================
 * *
 *                                                                                                *
 *                                       Xi Blockchain *
 *                                                                                                *
 * ----------------------------------------------------------------------------------------------
 * * This file is part of the Galaxia Project - Xi Blockchain *
 * ----------------------------------------------------------------------------------------------
 * *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the * GNU General Public License as published by the Free
 * Software Foundation, either version 3 of   * the License, or (at your option)
 * any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY;      * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.      * See the GNU General Public License
 * for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with
 * this program.     * If not, see <https://www.gnu.org/licenses/>. *
 *                                                                                                *
 * ==============================================================================================
 */

#include "Xi/Database/RocksDB/Database.hpp"

#include <algorithm>
#include <utility>
#include <iterator>

#include <rocksdb/db.h>

#include <Xi/FileSystem.h>

#include "Logger.hpp"
#include "Table.hpp"

#define XI_LOG_VERBOSE
#include <Xi/Log/Log.hpp>

XI_LOGGER("Database/RocksDB")

Xi::Database::RocksDB::SharedDatabase Xi::Database::RocksDB::Database::create(
    Xi::Database::RocksDB::Options options) {
  return SharedDatabase{new Database{options}};
}

Xi::Database::RocksDB::Database::Database(Xi::Database::RocksDB::Options options)
    : IDatabase(), m_options{std::move(options)} {
}

Xi::Database::RocksDB::Database::~Database() {
  XI_UNUSED_REVAL(doShutdown());
}

bool Xi::Database::RocksDB::Database::doInitialize() {
  auto nativeOptions = m_options.toNative();
  nativeOptions.info_log = std::make_shared<RocksDB::Logger>();
  nativeOptions.info_log_level = rocksdb::INFO_LEVEL;
  nativeOptions.create_if_missing = true;

  rocksdb::DB* handle = nullptr;
  std::string dataDir = m_options.dataDirectory.value_or("./data-dir/database");
  XI_DEBUG("Opening database: {}", dataDir);

  if (const auto ec = FileSystem::ensureDirectoryExists(dataDir)) {
    XI_FATAL("Unable to create database directory: {}", dataDir);
    return false;
  }

  if (const auto ec = rocksdb::DB::Open(nativeOptions, dataDir, &handle); !ec.ok()) {
    XI_FATAL("failed to open database: {}", ec.ToString());
    return false;
  }

  m_handle.reset(handle);

  XI_DEBUG("Database '{}' successfully opened.", dataDir);
  return true;
}

bool Xi::Database::RocksDB::Database::doShutdown() {
  try {
    if (m_handle) {
      rocksdb::FlushOptions options{};
      options.wait = true;
      m_handle->Flush(options);
      m_handle->Close();
      m_handle.reset();
    }
    return true;
  } catch (std::exception& e) {
    XI_FATAL("Shutdown threw: {}", e.what());
    return false;
  }
}

Xi::Database::UniqueITable Xi::Database::RocksDB::Database::doRequireTable(
    const Xi::Database::TablePrefix& prefix) {
  return std::make_unique<Table>(m_handle.get(), prefix);
}

bool Xi::Database::RocksDB::Database::doReleaseTable(Xi::Database::ITable& table) {
  static_cast<Table&>(table).release();
  return true;
}
