/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Database/RocksDB/Options.hpp"

#include <rocksdb/table.h>
#include <rocksdb/options.h>

rocksdb::Options Xi::Database::RocksDB::Options::toNative() const {
  rocksdb::DBOptions dbOptions;

  // See:
  // https://github.com/facebook/rocksdb/wiki/Setup-Options-and-Basic-Tuning#other-general-options
  dbOptions.max_background_compactions = 4;
  dbOptions.max_background_flushes = 2;
  dbOptions.bytes_per_sync = 1048576;
  dbOptions.max_background_compactions = 1;
  dbOptions.max_background_flushes = 1;

  if (threads.has_value()) {
    dbOptions.IncreaseParallelism(threads.value());
  }

  rocksdb::ColumnFamilyOptions fOptions;
  fOptions.write_buffer_size = writeBufferSize.value_or(64 << 20);
  fOptions.min_write_buffer_number_to_merge = 2;
  fOptions.max_write_buffer_number = 6;

  // See:
  // https://github.com/facebook/rocksdb/wiki/Setup-Options-and-Basic-Tuning#other-general-options
  fOptions.level_compaction_dynamic_level_bytes = true;
  fOptions.compaction_style = rocksdb::kCompactionStyleLevel;
  fOptions.level0_file_num_compaction_trigger = 10;
  fOptions.level0_slowdown_writes_trigger = 20;
  fOptions.level0_stop_writes_trigger = 40;

  fOptions.target_file_size_base = targetFileSize.value_or(fOptions.max_bytes_for_level_base / 10);
  fOptions.target_file_size_multiplier = targetFileSizeMultiplier.value_or(2);

  fOptions.num_levels = levels.value_or(10);
  fOptions.compression_per_level.resize(static_cast<uint16_t>(fOptions.num_levels));

  for (uint16_t i = 0; i < static_cast<uint16_t>(fOptions.num_levels); ++i) {
    fOptions.compression_per_level[i] = rocksdb::kNoCompression;
  }

  rocksdb::BlockBasedTableOptions tableOptions{};
  auto defaultedCacheStrategy = cacheStrategy.value_or(CacheStrategy::LastRecentlyUsed);
  auto defaultedCacheSize = cacheSize.value_or(128 << 20);
  if (defaultedCacheStrategy == CacheStrategy::None) {
    tableOptions.no_block_cache = true;
  } else if (defaultedCacheStrategy == CacheStrategy::LastRecentlyUsed) {
    tableOptions.block_cache = rocksdb::NewLRUCache(defaultedCacheSize);
  } else {
    tableOptions.block_cache = rocksdb::NewClockCache(defaultedCacheSize);
  }

  // See:
  // https://github.com/facebook/rocksdb/wiki/Setup-Options-and-Basic-Tuning#other-general-options
  tableOptions.block_size = 16 * 1024;
  tableOptions.cache_index_and_filter_blocks = true;
  tableOptions.pin_l0_filter_and_index_blocks_in_cache = true;

  fOptions.table_factory =
      std::shared_ptr<rocksdb::TableFactory>(NewBlockBasedTableFactory(tableOptions));

  return rocksdb::Options(dbOptions, fOptions);
}
