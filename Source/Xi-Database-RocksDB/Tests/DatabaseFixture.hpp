/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <random>
#include <map>
#include <set>

#include <gmock/gmock.h>

#include <Xi/Blob.hpp>
#include <Xi/Serialization/Serialization.hpp>
#include <Xi/Database/Database.hpp>
#include <Xi/Database/RocksDB/Database.hpp>

using namespace Xi;
using namespace Xi::Database;

struct Xi_Database_RocksDB_Blob : Xi::enable_blob_from_this<Xi_Database_RocksDB_Blob, 64> {
  using enable_blob_from_this::enable_blob_from_this;

  XI_SERIALIZATION_COMPLEX_BEGIN
  XI_RETURN_EC_IF_NOT(serializer.blob(span()), false);
  XI_SERIALIZATION_COMPLEX_END
};

class Xi_Database_RocksDB : public ::testing::Test {
 public:
  using BlobSet = TableTrait<0x0001, EntityTrait<void>, EntityTrait<Xi_Database_RocksDB_Blob, 64>>;
  using IndexedBlobs =
      TableTrait<0x0002, EntityTrait<uint32_t>, EntityTrait<Xi_Database_RocksDB_Blob, 64>>;
  using Strings_0 = TableTrait<0x0003, EntityTrait<std::string>, EntityTrait<std::string>>;
  using Strings_1 = TableTrait<0x0004, EntityTrait<std::string>, EntityTrait<std::string>>;

 public:
  void SetUp() override;
  void TearDown() override;

  std::map<IndexedBlobs::key_type, IndexedBlobs::value_type> insertRandomIndexedBlobs(size_t count);
  std::map<std::string, std::string> insertTestStrings();

  SharedTable<BlobSet> blobs();
  SharedTable<IndexedBlobs> blobIndices();
  SharedTable<Strings_0> strings_0();
  SharedTable<Strings_1> strings_1();

  void random(ByteSpan out) const;

  bool compare(const Xi_Database_RocksDB_Blob& lhs, const Xi_Database_RocksDB_Blob& rhs) const;

  SharedIDatabase database;
};
