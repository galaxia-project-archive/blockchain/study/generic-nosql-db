/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "DatabaseFixture.hpp"

#include <map>
#include <cstring>
#include <iterator>

TEST_F(Xi_Database_RocksDB, Put_Inserts) {
  auto table = blobIndices();
  auto blobs = insertRandomIndexedBlobs(20);

  for (const auto& iBlob : blobs) {
    auto value = table->get(iBlob.first).takeOrThrow();
    EXPECT_TRUE(compare(iBlob.second, value));
  }
}

TEST_F(Xi_Database_RocksDB, Get_ErrorsOnNoneExistingKey) {
  auto table = blobIndices();
  auto blobs = insertRandomIndexedBlobs(20);

  IndexedBlobs::value_type _;
  for (const auto& iBlob : blobs) {
    EXPECT_EQ(table->del(iBlob.first), QueryError::Success);
    EXPECT_EQ(table->get(iBlob.first, _), QueryError::NotFound);
    EXPECT_ANY_THROW(table->get(iBlob.first).throwOnError());
  }
}

TEST_F(Xi_Database_RocksDB, Put_Updates) {
  auto table = blobIndices();
  auto blobs = insertRandomIndexedBlobs(20);
  for (auto& iBlob : blobs) {
    random(iBlob.second.span());
    EXPECT_EQ(table->put(iBlob), QueryError::Success);
  }

  for (const auto& iBlob : blobs) {
    auto value = table->get(iBlob.first).takeOrThrow();
    EXPECT_TRUE(compare(iBlob.second, value));
  }
}

TEST_F(Xi_Database_RocksDB, Del_Range) {
  auto table = blobIndices();
  auto blobs = insertRandomIndexedBlobs(20);
  EXPECT_EQ(table->del(blobs.begin()->first, std::next(blobs.begin(), 10)->first),
            QueryError::Success);
  IndexedBlobs::value_type _;
  size_t count = 0;
  for (auto& iBlob : blobs) {
    if (count++ < 10) {
      EXPECT_EQ(table->get(iBlob.first, _), QueryError::NotFound);
    } else {
      EXPECT_EQ(table->get(iBlob.first, _), QueryError::Success);
    }
  }
}

TEST_F(Xi_Database_RocksDB, TablePrefixIsApplied) {
  auto table_0 = strings_0();
  auto table_1 = strings_1();
  auto strings = insertTestStrings();
  std::string _;
  for (auto& iString : strings) {
    ASSERT_EQ(table_0->get(iString.first, _), QueryError::Success);
    ASSERT_EQ(table_1->get(iString.first, _), QueryError::NotFound);
    ASSERT_EQ(table_1->put(iString.first, ""), QueryError::Success);
    EXPECT_EQ(table_0->get(iString.first).takeOrThrow(), iString.second);
  }
}
