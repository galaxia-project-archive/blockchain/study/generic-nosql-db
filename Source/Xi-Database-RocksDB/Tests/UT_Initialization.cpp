/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include "DatabaseFixture.hpp"

#include <Xi/Log/Log.hpp>
#include <Xi/Database/RocksDB/Database.hpp>

TEST_F(Xi_Database_RocksDB, Initialization_LockHoldsShutdown) {
  auto lck = blobIndices()->begin();
  (void)lck;

  EXPECT_FALSE(database->shutdown(std::chrono::milliseconds{200}));
}

TEST_F(Xi_Database_RocksDB, Initialization_LockReleaseExecutesShutdown) {
  {
    auto lck = blobIndices()->begin();
    (void)lck;

    EXPECT_FALSE(database->shutdown(std::chrono::milliseconds{200}));
    EXPECT_EQ(database->currentState(), IDatabase::State::ShuttingDown);
  }

  std::this_thread::sleep_for(std::chrono::milliseconds{200});
  EXPECT_EQ(database->currentState(), IDatabase::State::Closed);
}

TEST_F(Xi_Database_RocksDB, Initialization_PendingShutdownPreventsNewLocks) {
  auto lck = blobIndices()->begin();
  (void)lck;

  EXPECT_FALSE(database->shutdown(std::chrono::milliseconds{200}));
  auto busy = blobIndices()->get(0);
  ASSERT_TRUE(busy.isError());
  ASSERT_TRUE(busy.error().isErrorCode());
  EXPECT_EQ(static_cast<QueryError>(busy.error().errorCode().value()), QueryError::Busy);
}

TEST_F(Xi_Database_RocksDB, Intialization_DataIsPersistent) {
  auto blobs = insertRandomIndexedBlobs(20);
  auto table = blobIndices();
  for (const auto& iBlob : blobs) {
    auto value = table->get(iBlob.first).takeOrThrow();
    EXPECT_TRUE(compare(iBlob.second, value));
  }

  ASSERT_TRUE(database->shutdown());
  ASSERT_TRUE(database->initialize());

  IndexedBlobs::value_type _;
  for (const auto& iBlob : blobs) {
    ASSERT_EQ(table->get(iBlob.first, _), QueryError::Success);
    EXPECT_TRUE(compare(iBlob.second, _));
  }
}
