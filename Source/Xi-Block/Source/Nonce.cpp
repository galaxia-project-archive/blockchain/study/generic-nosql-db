/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Block/Nonce.hpp"

#include <Xi/Exceptions.hpp>
#include <Xi/Stream/InMemoryStreams.hpp>
#include <Xi/Encoding/Hex.hpp>

const Xi::Block::Nonce Xi::Block::Nonce::Null{{0, 0, 0, 0, 0, 0, 0, 0}};

bool Xi::Block::serialize(Xi::Block::Nonce &value, std::string_view name,
                          Serialization::Serializer &serializer) {
  return serializer.blob(value.span(), name);
}

std::string Xi::Block::toString(const Xi::Block::Nonce &nonce) {
  std::string reval;
  auto input = Stream::asInputStream(nonce.span());
  auto output = Stream::asOutputStream(reval);
  exceptional_if_not<RuntimeError>(Encoding::Hex::encode(*input, *output), "hex encoding failed");
  return reval;
}
