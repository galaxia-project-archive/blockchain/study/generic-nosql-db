# ============================================================================================== #
#                                                                                                #
#                                       Xi Blockchain                                            #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Galaxia Project - Xi Blockchain                                       #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Galaxia Project Developers                                                 #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

if(XI_CMAKE_HUNTER_PACKAGES_CMAKE)
  return()
endif()
set(XI_CMAKE_HUNTER_PACKAGES_CMAKE ON)

xi_include(Packages/ZLib)
xi_include(Packages/OpenSSL)
xi_include(Packages/Boost)
xi_include(Packages/Leathers)
xi_include(Packages/Fmt)
xi_include(Packages/RapidJson)
xi_include(Packages/RocksDB)

xi_include(GLOBAL Options)

if(XI_BUILD_TESTSUITE)
  xi_include(Packages/GTest)
endif()

if(XI_ENABLE_BREAKPAD)
  xi_include(Packages/Breakpad)
endif()
