﻿ # ============================================================================================== #
 #                                                                                                #
 #                                       Xi Blockchain                                            #
 #                                                                                                #
 # ---------------------------------------------------------------------------------------------- #
 # This file is part of the Galaxia Project - Xi Blockchain                                       #
 # ---------------------------------------------------------------------------------------------- #
 #                                                                                                #
 # Copyright 2018-2019 Galaxia Project Developers                                                 #
 #                                                                                                #
 # This program is free software: you can redistribute it and/or modify it under the terms of the #
 # GNU General Public License as published by the Free Software Foundation, either version 3 of   #
 # the License, or (at your option) any later version.                                            #
 #                                                                                                #
 # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
 # without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
 # See the GNU General Public License for more details.                                           #
 #                                                                                                #
 # You should have received a copy of the GNU General Public License along with this program.     #
 # If not, see <https://www.gnu.org/licenses/>.                                                   #
 #                                                                                                #
 # ============================================================================================== #

if(XI_CMAKE_COMPILER_FLAG_STACK_CMAKE)
  return()
endif()
set(XI_CMAKE_COMPILER_FLAG_STACK_CMAKE ON)

set(
  XI_COMPILER_STATE_VARS
    CMAKE_CXX_FLAGS
    CMAKE_CXX_FLAGS_DEBUG
    CMAKE_CXX_FLAGS_RELEASE
    CMAKE_CXX_FLAGS_RELWITHDEBINFO
    CMAKE_CXX_FLAGS_MINSIZEREL

    CMAKE_C_FLAGS
    CMAKE_C_FLAGS_DEBUG
    CMAKE_C_FLAGS_RELEASE
    CMAKE_C_FLAGS_RELWITHDEBINFO
    CMAKE_C_FLAGS_MINSIZEREL

    CMAKE_EXE_LINKER_FLAGS
    CMAKE_EXE_LINKER_FLAGS_DEBUG
    CMAKE_EXE_LINKER_FLAGS_RELEASE
    CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
    CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
)

macro(xi_compiler_stack_print)
  set(message "Current Compiler Configuration:")
  foreach(var ${XI_COMPILER_STATE_VARS})
    list(APPEND message "${var}: ${${var}}")
  endforeach()
  xi_debug(${message})
endmacro()

macro(xi_compiler_stack_push)
  foreach(state_var ${XI_COMPILER_STATE_VARS})
    list(INSERT "XI_COMPILER_STATE_STACK_${state_var}" 0 "${${state_var}}")
  endforeach()
endmacro()

macro(xi_compiler_stack_pop)
  foreach(state_var ${XI_COMPILER_STATE_VARS})
    list(GET "XI_COMPILER_STATE_STACK_${state_var}" 0 ${state_var})
    list(REMOVE_AT "XI_COMPILER_STATE_STACK_${state_var}" 0)
  endforeach()
endmacro()

